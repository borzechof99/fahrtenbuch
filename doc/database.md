# Changing the baked PSQL-Database to not include an email column for CheckIns

Because the email functionality of Check-Ins has been deprecated, no column for saving the user's adress is needed anymore. Hence, it shall be deleted. The TS-code referencing and defining the email column was removed on 14.09.2023, however, since the database was initialized with the email column, errors were still thrown when creating new check-in objects.

## How to remove a column by hand.

1. Enter the Postgres Docker with a shell
2. Connect to Postgres with `psql -U postgres`
3. List Databases with `\l`, connect to the relevant database with `\c fahrtenbuch`
4. List Tables with `\d`, list columns of a table with `\d <tablename>`
5. Remove a column from a table with `ALTER TABLE checkin DROP COLUMN IF EXISTS email;`
