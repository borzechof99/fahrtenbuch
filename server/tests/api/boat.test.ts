import {
	randomUUID
} from "crypto";
import request, {
	SuperAgentTest
} from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("Boat Route", () => {
	let server: SuperAgentTest;
	beforeAll(() => {
		server = request.agent("http://localhost:4000");
	});
	describe("Invalid JWT Requests", () => {
		it("should 401 (Unauthorized) when deleting a Boat", (done) => {
			server
				.delete("/api/boat/id_does_not_matter")
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when posting a Boat", (done) => {
			server
				.post("/api/boat/")
				.send({
					name: "Boat",
					boattype: "00000000-0000-0000-0000-000000000000", // randomUUID(),
					status: 1
				})
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when patching a Boat", (done) => {
			server
				.patch("/api/boat/does_not_matter")
				.send({
					name: "Boat",
					boattype: "00000000-0000-0000-0000-000000000000", // randomUUID(),
					status: 1
				})
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when locking a Boat", (done) => {
			server
				.post("/api/lock/does_not_matter")
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when unlocking a Boat", (done) => {
			server
				.post("/api/unlock/does_not_matter")
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
	});
	describe("Valid JWT Requests", () => {
		let boatId: string;
		let nonExistBoatId = "00000000-0000-0000-0000-000000000000"; // randomUUID();
		let sportId: string;
		let boatTypeId: string;
		beforeAll(async () => {
			await server
				.post("/api/login/")
				.send({
					email: email,
					password: password
				});
			// We need a sport for the boattype
			const sport = await server
				.post("/api/sport/")
				.send({
					name: "Test Sport"
				});
			sportId = sport.body.result.id;

			// We need to create a boattype first
			const boattype = await server
				.post("/api/boattype/")
				.send({
					name: "Test Boattype",
					seats: 4,
					sports: [{
						id: sportId
					}]
				});
			boatTypeId = boattype.body.result.id;
		});
		describe("Error Requests", () => {
			describe("404s", () => {
				it("should 404 (Not Found) when getting non existing boat", (done) => {
					server
						.get(`/api/boat/${nonExistBoatId}`)
						.expect(404)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.error).toEqual("boatIdNotFound");
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 404 (Not Found) when locking non existing boat", (done) => {
					server
						.post(`/api/lock/${nonExistBoatId}`)
						.expect(404)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.error).toEqual("boatIdNotFound");
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 404 (Not Found) when unlocking non existing boat", (done) => {
					server
						.post(`/api/unlock/${nonExistBoatId}`)
						.expect(404)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.error).toEqual("boatIdNotFound");
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 404 (Not Found) when patching non existing boat", (done) => {
					server
						.patch(`/api/boat/${nonExistBoatId}`)
						.send({
							name: "BoatyMcBoatface"
						})
						.expect(404)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.error).toEqual("boatIdNotFound");
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
			});
			describe("400s", () => {
				it("should 400 (Bad Request) when POST body contains no name", (done) => {
					server
						.post("/api/boat/")
						.send({
							boattype: "00000000-0000-0000-0000-000000000000", // randomUUID(),
							status: 1
						})
						.expect(400)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.errors).toEqual([{
								msg: 'Invalid value',
								param: 'name',
								location: 'body'
							}]);
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 400 (Bad Request) when POST body contains no boattype", (done) => {
					server
						.post("/api/boat/")
						.send({
							name: "BoatyMcBoatFace",
							status: 1
						})
						.expect(400)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.errors).toEqual([{
								msg: 'Invalid value',
								param: 'boattype',
								location: 'body'
							}]);
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 400 (Bad Request) when POST body contains no status", (done) => {
					server
						.post("/api/boat/")
						.send({
							name: "BoatyMcBoatFace",
							boattype: "00000000-0000-0000-0000-000000000000", // randomUUID()
						})
						.expect(400)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.errors).toEqual([{
								msg: 'Invalid value',
								param: 'status',
								location: 'body'
							}]);
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 400 (Bad Request) when PATCH body contains empty name", (done) => {
					server
						.patch(`/api/boat/${nonExistBoatId}`)
						.send({
							name: ""
						})
						.expect(400)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.errors).toEqual([{
								msg: 'Invalid value',
								param: 'name',
								location: 'body',
								value: ""
							}]);
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 400 (Bad Request) when PATCH body contains empty boattype", (done) => {
					server
						.patch(`/api/boat/${nonExistBoatId}`)
						.send({
							boattype: ""
						})
						.expect(400)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.errors).toEqual([{
								msg: 'Invalid value',
								param: 'boattype',
								location: 'body',
								value: ""
							}]);
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
				it("should 400 (Bad Request) when PATCH body contains empty status", (done) => {
					server
						.patch(`/api/boat/${nonExistBoatId}`)
						.send({
							status: ""
						})
						.expect(400)
						.expect((res) => {
							expect(res.body.success).toBeFalsy();
							expect(res.body.errors).toEqual([{
								msg: 'Invalid value',
								param: 'status',
								location: 'body',
								value: ""
							}]);
						})
						.end((err) => {
							if (err) return done(err);
							return done();
						});
				});
			});
		});
		it("should 201 (Created) when POSTing valid Boat", (done) => {
			server
				.post("/api/boat/")
				.send({
					name: "BoatyMcBoatFace",
					boattype: boatTypeId,
					status: 1
				})
				.expect(201)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result.id).toBeDefined();
					expect(res.body.result.name).toEqual("BoatyMcBoatFace");
					expect(res.body.result.status).toEqual(1);
					boatId = res.body.result.id;
					while (nonExistBoatId === boatId) nonExistBoatId = "00000000-0000-0000-0000-000000000000"; // randomUUID();
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});

		it("should 200 (OK) with result when GETting all boats", (done) => {
			server
				.get("/api/boat/")
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					//Since there could be multiple in this
					expect(res.body.result.length).toBeGreaterThan(0);
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 200 (OK) with result when getting specific boat", (done) => {
			server
				.get(`/api/boat/${boatId}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result.id).toEqual(boatId);
					expect(res.body.result.name).toEqual("BoatyMcBoatFace");
					expect(res.body.result.status).toEqual(1);
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 200 (OK) when PATCHing Boat", (done) => {
			server
				.patch(`/api/boat/${boatId}`)
				.send({
					"name": "Normal Boat"
				})
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result.id).toEqual(boatId);
					expect(res.body.result.name).toEqual("Normal Boat");
					expect(res.body.result.status).toEqual(1);
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 200 (OK) when locking boat", (done) => {
			server
				.post(`/api/lock/${boatId}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 200 (OK) when unlocking boat", (done) => {
			server
				.post(`/api/unlock/${boatId}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 200 (OK) when DELETEing Boat", (done) => {
			server
				.delete(`/api/boat/${boatId}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		afterAll(async () => {
			// Otherwise Sport and BoatType Tests could be polluted
			server
				.delete(`/api/boattype/${boatTypeId}`);
			server
				.delete(`/api/sport/${sportId}`);
		});
	});
});