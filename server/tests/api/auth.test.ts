import * as jwt from "jsonwebtoken";
import request, { SuperAgentTest } from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("Auth Route", () => {
	let server:SuperAgentTest;
	beforeAll(() => {
		server = request.agent("http://localhost:4000");
	});
	
	it("should 400 (Bad Request) without email", (done) => {
		server
		.post("/api/login/")
		.send({password: password})
		.expect(400)
		.expect("Content-Type", /json/)
		.expect((res) => {
			expect(res.body.success).toBeFalsy();
			expect(res.body.errors).toEqual([
				{msg: 'Invalid value', param: 'email', location: 'body'}
			]);
		})
		.end((err) => {
			if(err) return done(err);
			return done();
		});
	});
	it("should 400 (Bad Request) without password" , (done) => {
		server
		.post("/api/login/")
		.send({email: email})
		.expect(400)
		.expect("Content-Type", /json/)
		.expect((res) => {
			expect(res.body.success).toBeFalsy();
			expect(res.body.errors).toEqual([
				{msg: 'Invalid value', param: 'password', location: 'body'}
			]);
		})
		.end((err) => {
			if(err) return done(err);
			return done();
		});
	});

	it("should 401 (Unauthorized) with invalid email", (done) => {
		server
		.post("/api/login/")
		.send({email: email.substring(0, email.length - 1), password: password})
		.expect(401)
		.expect("Content-Type", /json/)
		.expect((res) => {
			expect(res.body.success).toBeFalsy();
			expect(res.body.error).toEqual("invalidEmail");
		})
		.end((err) => {
			if(err) return done(err);
			return done();
		});
	});
	
	it("should 401 (Unauthorized) with invalid password", (done) => {
		server
		.post("/api/login/")
		.send({email: email, password: password.substring(0, password.length - 1)})
		.expect(401)
		.expect("Content-Type", /json/)
		.expect((res) => {
			expect(res.body.success).toBeFalsy();
			expect(res.body.error).toEqual("invalidPassword");
		})
		.end((err) => {
			if(err) return done(err);
			return done();
		});
	});

	it("should 200 with right data", (done) => {
		server
		.post("/api/login")
		.send({email: email, password: password})
		.expect(200)
		.expect("Content-Type", /json/)
		.expect((res) => {
			//Checks if success and if cookie wants to be set
			expect(res.body.success).toBeTruthy();
			expect(res.header["set-cookie"]).toBeDefined();
			//Validates that return is actual JWT
			const token = res.header["set-cookie"][0]
							.split(";")[0]
							.replace("token=", "");
			expect(jwt.verify(token, process.env.JWT_SECRET)).toBeTruthy();
		})
		.end((err) => {
			if(err) return done(err);
			return done();
		});
	});
});