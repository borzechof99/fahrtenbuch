import {
	randomUUID
} from "crypto";
import request, {
	SuperAgentTest
} from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("Check In Route", () => {
	let server: SuperAgentTest;
	beforeAll(() => {
		server = request.agent("http://localhost:4000");
	});
	// DELETE not needed at the moment
	// describe("Invalid JWT Requests", () => {
	// 	it("should 401 (Unauthorized) when DELETEing a Check In", (done) => {
	// 		server
	// 			.delete("/api/checkin/does_not_matter")
	// 			.expect(401)
	// 			.expect((res) => {
	// 				expect(res.body.success).toBeFalsy();
	// 				expect(res.body.error).toBe('InvalidToken');
	// 			})
	// 			.end((err) => {
	// 				if (err) return done(err);
	// 				return done();
	// 			});
	// 	});
	// });
	describe("Valid JWT Requests", () => {
		let checkInId: string;
		let nonExistcheckInId = "00000000-0000-0000-0000-000000000000"; // randomUUID();
		let sportId: string;
		let boatTypeId: string;
		let boatId: string;
		beforeAll(async () => {
			await server
				.post("/api/login/")
				.send({
					email: email,
					password: password
				});
			// We need a sport for the boattype
			const sport = await server
				.post("/api/sport/")
				.send({
					name: "Test Sport"
				});
			sportId = sport.body.result.id;
			const boatType = await server
				.post("/api/boattype/")
				.send({ name: "Test Boat Type", seats: 4, sports: [{ id: sportId }] });
			boatTypeId = boatType.body.result.id;
			const boat = await server
				.post("/api/boat/")
				.send({ name: "Test Boat", boattype: boatTypeId, status: 0 });
			boatId = boat.body.result.id;
		});
		describe("Error Requests", () => {
			it("should 404 when GETting CheckIn from non existing", (done) => {
				server
					.get(`/api/checkin/${nonExistcheckInId}`)
					.expect(404)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.error).toBe('checkInIdNotFound');
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 404 when checkout non existing checkIn", (done) => {
				server
					.post(`/api/checkout/${checkInId}`)
					.expect(404)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.error).toBe('checkInIdNotFound');
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			// DELETE not needed at the moment
			// it("should 404 when DELETEing non existing Boat Type", (done) => {
			// 	server
			// 		.delete(`/api/checkin/${nonExistcheckInId}`)
			// 		.expect(404)
			// 		.expect((res) => {
			// 			expect(res.body.success).toBeFalsy();
			// 			expect(res.body.error).toBe('checkInIdNotFound');
			// 		})
			// 		.end((err) => {
			// 			if (err) return done(err);
			// 			return done();
			// 		});
			// });
			it("should 400 (Bad Request) when POSTing without startTime", (done) => {
				server
					.post("/api/checkin/")
					.send({
						estimatedEndTime: new Date().toISOString(),
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						accessory: "",
					})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{
							msg: 'Invalid value',
							param: 'startTime',
							location: 'body'
						}]);
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 400 (Bad Request) when POSTing without endTime", (done) => {
				server
					.post("/api/checkin/")
					.send({
						startTime: new Date().toISOString(),
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						accessory: "",
					})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toContainEqual({
							msg: 'Invalid value',
							param: 'estimatedEndTime',
							location: 'body'
						});
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 400 (Bad Request) when POSTing with no destination", (done) => {
				server
					.post("/api/checkin/")
					.send({
						startTime: new Date().toISOString(),
						estimatedEndTime: new Date().toISOString(),
						fullNameOfResponsibleClient: "Me",
						accessory: "",
					})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toContainEqual({
							msg: 'Invalid value',
							param: 'destination',
							location: 'body',
						});
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 400 (Bad Request) when POSTing without fullNameOfResponsibleClient", (done) => {
				server
					.post("/api/checkin/")
					.send({
						startTime: new Date().toISOString(),
						estimatedEndTime: new Date().toISOString(),
						destination: "Wannsee",
						accessory: "",
					})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toContainEqual({
							msg: 'Invalid value',
							param: 'fullNameOfResponsibleClient',
							location: 'body'
						});
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 400 (Bad Request) when POSTing with Time not in ISO8601", (done) => {
				let falseDate = new Date().toString();
				server
					.post("/api/checkin/")
					.send({
						startTime: falseDate,
						estimatedEndTime: new Date().toISOString(),
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						accessory: "",
					})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toContainEqual({
							msg: 'Invalid value',
							param: 'startTime',
							location: 'body',
							value: falseDate
						});
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 400 (Bad Request) when POSTing with endTime not in ISO8601", (done) => {
				let falseDate = new Date().toString();
				server
					.post("/api/checkin/")
					.send({
						startTime: new Date().toISOString(),
						estimatedEndTime: falseDate,
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						accessory: "",
					})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toContainEqual({
							msg: 'Invalid value',
							param: 'estimatedEndTime',
							location: 'body',
							value: falseDate
						});
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 404 (Not found) when POSTing with invalid boat", (done) => {
				server
					.post("/api/checkin/")
					.send({
						startTime: new Date().toISOString(),
						estimatedEndTime: new Date().toISOString(),
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						boatName: "00000000-0000-0000-0000-000000000000", //randomUUID()
						accessory: "",
					})
					.expect(404)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.error).toEqual("boatIdNotFound");
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 404 (Not found) when POSTing with invalid sport", (done) => {
				server
					.post("/api/checkin/")
					.send({
						startTime: new Date().toISOString(),
						estimatedEndTime: new Date().toISOString(),
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						boatName: boatId,
						sport: "00000000-0000-0000-0000-000000000000", // randomUUID()
						accessory: "",
					})
					.expect(404)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.error).toEqual("sportIdNotFound");
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});
			it("should 201 (Created) when POSTing", (done) => {
				let time = new Date().toISOString();
				server
					.post("/api/checkin/")
					.send({
						startTime: time,
						estimatedEndTime: time,
						destination: "Wannsee",
						fullNameOfResponsibleClient: "Me",
						boatName: boatId,
						sport: sportId,
						accessory: "",
					})
					.expect(201)
					.expect((res) => {
						expect(res.body.success).toBeTruthy();
						expect(res.body.result.id).toBeDefined();
						expect(res.body.result.startTime).toEqual(time);
						expect(res.body.result.estimatedEndTime).toEqual(time);
						expect(res.body.result.destination).toEqual("Wannsee");
						expect(res.body.result.fullNameOfResponsibleClient).toEqual("Me");
						expect(res.body.result.boatName).toEqual(boatId);
						expect(res.body.result.sport).toEqual(sportId);
						checkInId = res.body.result.id;
						while (nonExistcheckInId === checkInId) nonExistcheckInId = "00000000-0000-0000-0000-000000000000"; // randomUUID();
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			})
			it("should 200 (OK) when check out", (done) => {
				server
					.post(`/api/checkout/${checkInId}/`)
					.expect(200)
					.expect((res) => {
						expect(res.body.success).toBeTruthy();
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});

			it("should 200 (OK) when GETting check in", (done) => {
				server
					.get(`/api/checkin/${checkInId}/`)
					.expect(200)
					.expect((res) => {
						expect(res.body.success).toBeTruthy();
						expect(res.body.result.id).toEqual(checkInId);
					})
					.end((err) => {
						if (err) return done(err);
						return done();
					});
			});

			// DELETE not needed at the moment
			/* 
			it("should 200 (OK) when DELETEing check in", (done) => {
				server
				.delete(`/api/checkin/${checkInId}/`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			*/

			afterAll(() => {
				server
					.delete(`/api/boat/${boatId}/`);
				server
					.delete(`/api/boattype/${boatTypeId}/`);
				server
					.delete(`/api/sport/${sportId}/`);
			})
		});
	});
});