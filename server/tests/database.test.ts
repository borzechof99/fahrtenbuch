import { Client } from "pg";
require("dotenv").config();

it("has the database", async () => {
  const client = new Client({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    password: process.env.POSTGRES_PASSWORD,
  });
  await client.connect();
  const query = await client.query(
    `SELECT * FROM pg_database WHERE datname='${process.env.POSTGRES_DB}'`
  );

  expect(query.rows.length).toBeGreaterThan(0);

  await client.end();
});

describe("Database Tests", () => {
  let client: Client;
  const databaseTables = [
    {
      table_name: "checkin",
      columns: [
        { column_name: "id", data_type: "uuid" },
        { column_name: "startTime", data_type: "timestamp with time zone" },
        {
          column_name: "estimatedEndTime",
          data_type: "timestamp with time zone",
        },
        { column_name: "boatId", data_type: "uuid" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "email", data_type: "text" },
        {
          column_name: "fullNameOfResponsibleClient",
          data_type: "text",
        },
        { column_name: "additionalClients", data_type: "ARRAY" },
        { column_name: "numP", data_type: "integer" },
        { column_name: "date", data_type: "date" },
        { column_name: "returned", data_type: "boolean" },
        { column_name: "isCourse", data_type: "boolean" },
        { column_name: "destination", data_type: "text" },
        { column_name: "note", data_type: "text" },
        { column_name: "noteDone", data_type: "boolean" },
        { column_name: "bookingType", data_type: "text" },
        { column_name: "SportId", data_type: "uuid" },
        { column_name: "BoatId", data_type: "uuid" },
        { column_name: "AccessoryId", data_type: "uuid" },
        { column_name: "adminMemo", data_type: "text" }
      ],
    },
    {
      table_name: "boat",
      columns: [
        { column_name: "status", data_type: "integer" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "BoatTypeId", data_type: "uuid" },
        { column_name: "id", data_type: "uuid" },
        { column_name: "name", data_type: "text" },
        { column_name: "maxTime", data_type: "double precision" },
      ],
    },
    {
      table_name: "boattype",
      columns: [
        { column_name: "id", data_type: "uuid" },
        { column_name: "seats", data_type: "integer" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "name", data_type: "text" },
        { column_name: "maxTime", data_type: "double precision" },
      ],
    },
    {
      table_name: "employee",
      columns: [
        { column_name: "id", data_type: "uuid" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "last_name", data_type: "text" },
        { column_name: "password", data_type: "text" },
        { column_name: "role", data_type: "text" },
        { column_name: "email", data_type: "text" },
        { column_name: "first_name", data_type: "text" },
      ],
    },
    {
      table_name: "sport",
      columns: [
        { column_name: "id", data_type: "uuid" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "name", data_type: "text" },
        { column_name: "color", data_type: "text" },
      ],
    },
    {
      table_name: "Sport_BoatType",
      columns: [
        { column_name: "BoatTypeId", data_type: "uuid" },
        { column_name: "SportId", data_type: "uuid" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
      ]
    },
    {
      table_name: "BoatType_AccessoryType",
      columns: [
        { column_name: "BoatTypeId", data_type: "uuid" },
        { column_name: "AccessoryTypeId", data_type: "uuid" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
      ]
    },
    {
      table_name: "accessorytype",
      columns: [
        { column_name: "id", data_type: "uuid" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "name", data_type: "text" },
      ],
    },
    {
      table_name: "accessory",
      columns: [
        { column_name: "status", data_type: "integer" },
        { column_name: "createdAt", data_type: "timestamp with time zone" },
        { column_name: "updatedAt", data_type: "timestamp with time zone" },
        { column_name: "AccessoryTypeId", data_type: "uuid" },
        { column_name: "id", data_type: "uuid" },
        { column_name: "name", data_type: "text" },
      ],
    },
  ];
  beforeAll(async () => {
    client = new Client({
      user: process.env.POSTGRES_USER,
      host: process.env.POSTGRES_HOST,
      database: process.env.POSTGRES_DB,
      password: process.env.POSTGRES_PASSWORD,
    });
    await client.connect();
  });

  afterAll(async () => await client.end());

  it("has the correct tables", async () => {
    const query = await client.query(
      `SELECT table_name FROM information_schema.tables WHERE table_schema='public'`
    );
    const tableNames = query.rows.map((row) => row.table_name).sort();
    const shouldHaveTableNames = databaseTables
      .map((table) => table.table_name)
      .sort();

    expect(tableNames).toEqual(shouldHaveTableNames);
  });

  it.each(databaseTables)(
    "has correct columns for each table",
    async ({ table_name, columns }) => {
      const query = await client.query(
        `SELECT column_name, data_type FROM information_schema.columns WHERE table_name='${table_name}';`
      );
      const queryColumns = query.rows;

      for (let queryColumn of queryColumns) {
        expect(columns).toContainEqual(queryColumn);
      }
    }
  );

  it("has the initial employee", async () => {
    const query = await client.query(
      `SELECT * FROM employee WHERE email='${process.env.INITIAL_COORDINATOR_EMAIL}';`
    );

    expect(query.rows.length).toEqual(1);
  });
});
