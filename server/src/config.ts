import { Dialect } from "sequelize";

const envVars = {
  DB_NAME: process.env.POSTGRES_DB,
  DB_USER: process.env.POSTGRES_USER,
  DB_HOST: process.env.POSTGRES_HOST,
  DB_DRIVER: process.env.POSTGRES_DRIVER as Dialect,
  DB_PASSWORD: process.env.POSTGRES_PASSWORD,
  JWT_SECRET: process.env.JWT_SECRET,
  INITIAL_COORDINATOR_PASSWORD: process.env.INITIAL_COORDINATOR_PASSWORD,
  INITIAL_COORDINATOR_EMAIL: process.env.INITIAL_COORDINATOR_EMAIL,
};
if (Object.values(envVars).every((value) => value === undefined)){
  console.error("Error while reading DB Environment variables! Is maybe the .env file missing?");
  process.exit(9);
} else if (Object.values(envVars).findIndex((value) => value === undefined) !== -1) {
  let missingVars = Object.entries(envVars).filter(([key,value])=> value === undefined);
  console.error(`Error while reading DB Environment variables!\n ${missingVars.map(x=>"  - " + x[0] + "is missing.\n")} `);
  process.exit(9);
}

export default envVars;
