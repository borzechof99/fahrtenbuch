import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import Employee from "../db/models/Employee";
import envVars from "../config";


export function validateTokenGenerator(validRoleList = ["coordinator", "boatManager"]) {

  async function validateToken(req: Request, res: Response, next: NextFunction) {
    const token = req.cookies.token;

    //Try to validate the token and get data
    try {
      const jwtPayload = jwt.verify(token, envVars.JWT_SECRET);
      res.locals.user = jwtPayload;
      const employee = await Employee.findOne({
        where: {
          email: res.locals.user.email,
        }
      });
      if (employee && validRoleList.includes(employee.role)) {
        next();
      }
      else {
        res.locals.user = null;
        return res.status(401).cookie("token", null, {
          httpOnly: true,
          secure: process.env.NOVE_ENV === "production",
          path: "/",
          expires: new Date(0),
        }).json({ success: false, error: "InvalidToken" });

      }
    } catch (error) {
      //If token is not valid, respond with 401 (unauthorized)
      return res.status(401).json({ success: false, error: "InvalidToken" });
    }
  }

  return validateToken
}


export function validateCheckinToken(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const token = req.cookies["checkin_token"];

  try {
    const jwtPayload = jwt.verify(token, envVars.JWT_SECRET);
    res.locals.checkinToken = jwtPayload;
    next();
  } catch (error) {
    //If token is not valid, respond with 401 (unauthorized)
    return res.status(401).json({ success: false, error: "InvalidToken" });
  }
}
