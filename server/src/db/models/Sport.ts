import { Sequelize, DataTypes, Model, Optional, HasManyGetAssociationsMixin } from "sequelize";
import CheckIn from "./CheckIn";

interface SportAttributes {
  id: string;
  name: string;
  color: string;
}
export interface SportInput extends Optional<SportAttributes, "id"> { }

class Sport
  extends Model<SportAttributes, SportInput>
  implements SportAttributes {
  declare id: string;
  declare name: string;
  declare color: string;

  public getCheckIns!: HasManyGetAssociationsMixin<CheckIn>;


  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;
}

export const initSport = (sequelizeConnection: Sequelize) => {
  Sport.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
      color: {
        type: new DataTypes.TEXT(),
        allowNull: true,
      },
    },
    {
      tableName: "sport",
      sequelize: sequelizeConnection,
    }
  );
};
export default Sport;
