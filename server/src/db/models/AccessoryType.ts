import {
    Sequelize,
    DataTypes,
    Model,
    Optional,
    Association,
    HasManyAddAssociationMixin,
    BelongsToManySetAssociationsMixin,
    BelongsToManyGetAssociationsMixin
} from "sequelize";
import BoatType from "./BoatType";
import Accessory from "./Accessory";

interface AccessoryTypeAttributes {
    id: string;
    name: string;
}
export interface AccessoryTypeInput extends Optional<AccessoryTypeAttributes, "id"> { }

class AccessoryType
    extends Model<AccessoryTypeAttributes, AccessoryTypeInput>
    implements AccessoryTypeAttributes {
    declare id: string;
    declare name: string;

    declare readonly createdAt: Date;
    declare readonly updatedAt: Date;

    public addBoatType: HasManyAddAssociationMixin<BoatType, any>;
    public setBoatTypes!: BelongsToManySetAssociationsMixin<BoatType, number>
    public getAccessories!: BelongsToManyGetAssociationsMixin<Accessory>
    declare readonly boattypes?: BoatType[];
    declare readonly accessories?: Accessory[];

    declare static associations: {
        boattypes: Association<AccessoryType, BoatType>;
    };
}

export const initAccessoryType = (sequelizeConnection: Sequelize) => {
    AccessoryType.init(
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: new DataTypes.TEXT(),
                allowNull: false,
            }
        },
        {
            tableName: "accessorytype",
            sequelize: sequelizeConnection,
        }
    );
};
export default AccessoryType;
