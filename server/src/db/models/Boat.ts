import { Sequelize, DataTypes, Model, Optional, HasManyGetAssociationsMixin } from "sequelize";
import CheckIn from "./CheckIn";

interface BoatAttributes {
  id: string;
  name: string;
  status: number;
  BoatTypeId?: string;
}

export interface BoatAttributesInput extends Optional<BoatAttributes, "id"> { }

class Boat
  extends Model<BoatAttributes, BoatAttributesInput>
  implements BoatAttributes {
  declare id: string;
  declare name: string;
  declare status: number;

  declare BoatTypeId?: string;
  public getCheckIns!: HasManyGetAssociationsMixin<CheckIn>;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;
}

export const initBoat = (sequelizeConnection: Sequelize) => {
  Boat.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
      status: {
        type: DataTypes.INTEGER,
        allowNull: false,
      }
    },
    {
      tableName: "boat",
      sequelize: sequelizeConnection,
    }
  );
};

export default Boat;
