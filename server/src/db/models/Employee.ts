import { DataTypes, Model, Optional, Sequelize } from "sequelize";

interface EmployeeAttributes {
  id: string;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
  role: string;
}
export interface EmployeeInput extends Optional<EmployeeAttributes, "id"> { }
class Employee
  extends Model<EmployeeAttributes, EmployeeInput>
  implements EmployeeAttributes {
  declare id: string;
  declare email: string;
  declare first_name: string;
  declare last_name: string;
  declare password: string;
  declare role: string;
  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;
}
export const initEmployee = (sequelizeConnection: Sequelize) => {
  Employee.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      email: {
        type: new DataTypes.TEXT(),
        allowNull: false,
        unique: true,
      },
      first_name: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
      last_name: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
      password: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
      role: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
    },
    {
      tableName: "employee",
      sequelize: sequelizeConnection,
    }
  );
};

export default Employee;
