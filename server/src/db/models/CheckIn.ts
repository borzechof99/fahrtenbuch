import { DataTypes, Model, Optional, Sequelize } from "sequelize";

interface CheckInAttributes {
  id: string;
  date: Date;
  startTime: Date;
  estimatedEndTime: Date;
  destination: string;
  fullNameOfResponsibleClient: string;
  additionalClients: Array<string>;
  returned: boolean;
  note: string;
  noteDone: boolean;
  bookingType: string;
  adminMemo: string;
  isCourse: boolean;

  SportId?: string;
  BoatId?: string;
  AccessoryId?: string;
}
export interface CheckInAttributesInput
  extends Optional<CheckInAttributes, "id"> { }

class CheckIn
  extends Model<CheckInAttributes, CheckInAttributesInput>
  implements CheckInAttributes {
  declare id: string;
  declare date: Date;
  declare destination: string;
  declare startTime: Date;
  declare estimatedEndTime: Date;
  declare fullNameOfResponsibleClient: string;
  declare additionalClients: Array<string>;
  declare returned: boolean;
  declare note: string;
  declare noteDone: boolean;
  declare bookingType: string;
  declare adminMemo: string;
  declare isCourse: boolean;

  declare SportId?: string;
  declare BoatId?: string;
  declare AccessoryId?: string;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;
}

export const initCheckIn = (sequelizeConnection: Sequelize) => {
  CheckIn.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      destination: {
        type: DataTypes.TEXT(),
        allowNull: true,
      },
      startTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      estimatedEndTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      fullNameOfResponsibleClient: {
        type: new DataTypes.TEXT(),
        allowNull: false,
      },
      additionalClients: {
        type: new DataTypes.ARRAY(DataTypes.TEXT),
      },
      bookingType: {
        type: DataTypes.TEXT(),
        allowNull: true,
      },
      returned: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      note: {
        type: DataTypes.TEXT(),
        allowNull: true,
      },
      noteDone: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
      },
      adminMemo: {
        type: DataTypes.TEXT(),
        allowNull: true,
      },
      isCourse: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      tableName: "checkin",
      sequelize: sequelizeConnection,
    }
  );
};

export default CheckIn;
