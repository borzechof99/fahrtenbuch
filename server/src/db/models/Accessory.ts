import { Sequelize, DataTypes, Model, Optional, HasManyGetAssociationsMixin } from "sequelize";
import CheckIn from "./CheckIn";

interface AccessoryAttributes {
    id: string;
    name: string;
    status: number;
    AccessoryTypeId?: string;
}

export interface AccessoryAttributesInput extends Optional<AccessoryAttributes, "id"> { }

class Accessory
    extends Model<AccessoryAttributes, AccessoryAttributesInput>
    implements AccessoryAttributes {
    declare id: string;
    declare name: string;
    declare status: number;

    declare AccessoryTypeId?: string;
    public getCheckIns!: HasManyGetAssociationsMixin<CheckIn>;

    declare readonly createdAt: Date;
    declare readonly updatedAt: Date;
}

export const initAccessory = (sequelizeConnection: Sequelize) => {
    Accessory.init(
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: new DataTypes.TEXT(),
                allowNull: false,
            },
            status: {
                type: DataTypes.INTEGER,
                allowNull: false,
            }
        },
        {
            tableName: "accessory",
            sequelize: sequelizeConnection,
        }
    );
};

export default Accessory;
