import bcrypt from "bcrypt";
import { Request, Response } from "express";
import Employee from "../db/models/Employee";
//create new account
const createAccountController = async (req: Request, res: Response) => {
  try {
    //get input from req.body
    const input = req.body;

    //get all accounts with same given email
    const existedAccount = await Employee.findAll({
      where: {
        email: input.email,
      },
    });

    //if account with same given email was found
    if (existedAccount.length > 0) {
      return res
        .status(409)
        .json({ success: false, error: "AccountAlreadyExists" });
    }

    //encrypt given password
    input.password = await bcrypt.hash(input.password, 10);

    //creating new Account
    const newAccount = await Employee.create(input);

    //returning result
    return res.status(201).json({
      success: true,
      account: {
        id: newAccount.id,
        first_name: newAccount.first_name,
        last_name: newAccount.last_name,
        email: newAccount.email,
        role: newAccount.role,
      },
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};
//show all accounts
const showAllAccounts = async (req: Request, res: Response) => {
  try {
    const allAccounts = await Employee.findAll();
    return res.status(200).json({
      success: true,
      result: allAccounts.map((account) => {
        return {
          id: account.id,
          first_name: account.first_name,
          last_name: account.last_name,
          email: account.email,
          role: account.role, // The role Strings are defined _only_ in the Frontend Staff Account Creation process!!
        };
      }),
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

//show a specific account using given id: email is the primary key for a Employee
const showAccountById = async (req: Request, res: Response) => {
  try {
    const givenId = req.params.id;
    const account = await Employee.findByPk(givenId);
    if (account) {
      return res.status(200).json({
        success: true,
        result: {
          id: account.id,
          first_name: account.first_name,
          last_name: account.last_name,
          email: account.email,
          role: account.role,
        },
      });
    }
    return res.status(404).json({ success: false, error: "accountIdNotFound" });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

//update account by id
const updateAccount = async (req: Request, res: Response) => {
  //by trying to update email, duplicates could be found which leads to server-error: must be handled seperately?
  try {
    //take what ever in req.body is, and pass it to update()
    const input = req.body;

    //return 200 with empty response if no data was given
    if (Object.keys(input).length === 0) {
      return res
        .status(200)
        .json({ success: true, result: {}, message: "noInputFound" });
    }

    //get given id
    const givenId = req.params.id;

    //check if given ID exists in DB
    const foundEmployee = await Employee.findByPk(givenId);

    if (foundEmployee === null) {
      return res.status(404).json({ success: false, error: "accountNotFound" });
    }

    //hash given password
    if (input.password) {
      input.password = await bcrypt.hash(input.password, 10);
    } else {
      input.password = foundEmployee.password;
    }

    //update Employee
    const userDataAfterUpdate = await Employee.update(input, {
      where: {
        id: givenId, //primary key in Employee is email
      },
      returning: true,
    });

    const userData = userDataAfterUpdate[1][0];
    delete userData.password;

    return res.status(200).json({
      success: true,
      result: {
        id: userData.id,
        first_name: userData.first_name,
        last_name: userData.last_name,
        email: userData.email,
        role: userData.role,
      },
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

//delete specific account using given id
const deleteAccountById = async (req: Request, res: Response) => {
  //what happens by deleting own-account (coordinator-account)?

  try {
    //delete using given id (email)
    const givenId = req.params.id;
    //not allowed to delete the last coordinator
    //all coordinators
    const coordinators = await Employee.findAll({
      where:{
        role: "coordinator"
      }
    })
    //check if givenId is coordinator
    if (coordinators.some(e => e.id === givenId)) {    
    //if we have only one last coordinator-> return with fail
      if(coordinators.length < 2){
        return res
        .status(400)
        .json({success: false, error:"canNotDeleteLastCoordinator"})
    }}
    //otherwise
    const accountToDelete = await Employee.destroy({
      where: {
        id: givenId,
      },
    });

    if (accountToDelete == 0) {
      return res
        .status(404)
        .json({ success: false, error: "accountIdDoesNotExist" });
    }

    return res.status(200).json({ success: true });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const accountsControllers = {
  createAccountController,
  showAllAccounts,
  showAccountById,
  updateAccount,
  deleteAccountById,
};

export default accountsControllers;
