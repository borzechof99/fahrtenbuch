import { Request, Response } from "express";
import { Op, Sequelize } from "sequelize";
import CheckIn from "../db/models/CheckIn";
import Boat from "../db/models/Boat";
import Sport from "../db/models/Sport";
import jwt from "jsonwebtoken";
import envVars from "../config";
import sendMail from "../mail";
import Employee from "../db/models/Employee";
import Accessory from "../db/models/Accessory";

export const checkInController = async (req: Request, res: Response) => {
  /** Function for lending out a boat to a user.
   *  Sends a cookie to the user's browser including the ID of the checkIn
   *  This allows the boat to be given back at a later time through the function checkOutController()
   */

  try {
    // Unpack all Data from Request
    const {
      sport,
      boatName,
      accessory,
      startTime,
      estimatedEndTime,
      destination,
      additionalClients,
      fullNameOfResponsibleClient,
      isCourse,
    }: {
      sport: string;
      boatName: string;
      accessory: string;
      startTime: Date;
      estimatedEndTime: Date;
      destination: string;
      additionalClients: string[];
      fullNameOfResponsibleClient: string;
      isCourse: boolean;
    } = req.body;

    // Convert boatName string to Boat Object
    const boat = await Boat.findByPk(boatName);

    // In case boat was deleted during Checkin Process
    if (!boat) {
      return res.status(404).json({ success: false, error: "boatIdNotFound" });
    }

    // Do same checks with Accessory as with Boat
    let accessoryObj = null;
    let accessoryStatusBool = false;
    if (accessory === undefined || accessory === null || accessory !== "") {
      accessoryObj = await Accessory.findByPk(accessory);

      if (!accessoryObj) {
        return res.status(404).json({ success: false, error: "accessoryIdNotFound" });
      }
      accessoryStatusBool = accessoryObj.status !== 0;
    }

    // If the boat (or Accessory) was checked in by another user during the Checkin Process, another CheckIn-Object could be found
    let checkInsWithBoat = (accessory === "") ?
      (
        await CheckIn.findAll({
          where: { returned: false, BoatId: boatName },
        })
      )
      : (
        await CheckIn.findAll({
          where: Sequelize.and({ returned: false }, Sequelize.or({ BoatId: boatName }, { AccessoryId: accessory })),
        })
      );


    // boat.status reports whether the boat is blocked or not
    if (boat.status !== 0 || accessoryStatusBool || checkInsWithBoat.length > 0) {
      return res
        .status(400)
        .json({ success: false, error: "Boat or Accessory not Available" });
    }

    // Find Sport, convert from String to Sport-Object
    const sportObj = await Sport.findByPk(sport);

    if (!sportObj) {
      return res.status(404).json({ success: false, error: "sportIdNotFound" });
    }


    const rentedAt = new Date();

    const newLogEntry = await CheckIn.create({
      SportId: sport,
      BoatId: boatName,
      AccessoryId: accessory === "" ? undefined : accessory,
      startTime,
      estimatedEndTime,
      destination,
      fullNameOfResponsibleClient: fullNameOfResponsibleClient,
      additionalClients: additionalClients,
      bookingType: "default", // TODO: Are there different Booking Types?
      returned: false,
      note: null,
      date: rentedAt,
      adminMemo: null,
      isCourse,
    });

    const payload = {
      id: newLogEntry.id,
    };

    const token = jwt.sign(payload, envVars.JWT_SECRET);

    let endOfDay = new Date();
    // Setzt die Uhrzeit auf 23:30:00, was dem Ende des Tages entspricht.
    endOfDay.setHours(23, 30, 0);

    //return result after checking all possible error-cases
    return res
      .cookie("checkin_token", token, {
        secure: process.env.NODE_ENV === "production",
        path: "/",
        expires: endOfDay,
      })
      .status(201)
      .json({
        success: true,
        result: {
          id: newLogEntry.id,
          sport,
          boatName,
          accessory,
          startTime,
          estimatedEndTime,
          destination,
          fullNameOfResponsibleClient,
          additionalClients,
          bookingType: "default",
          returned: false,
          note: null,
          date: rentedAt,
          adminMemo: null,
          isCourse,
        },
      });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

export const checkInNoteDoneController = async (req: Request, res: Response) => {
  try {
    const checkIn = await CheckIn.findByPk(req.params.id);
    if (checkIn) {
      await CheckIn.update({
        noteDone: true
      },
        {
          where: {
            id: req.params.id,
          },
          returning: false
        });
      return res.status(200).json({ success: true });
    }
    return res.status(404).json({ success: false, error: "checkInNotFound" });
  }
  catch (error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
};

export const checkInNoteNotDoneController = async (req: Request, res: Response) => {
  try {
    const checkIn = await CheckIn.findByPk(req.params.id);
    if (checkIn) {
      await CheckIn.update({
        noteDone: false
      },
        {
          where: {
            id: req.params.id,
          },
          returning: false
        });
      return res.status(200).json({ success: true });
    }
    return res.status(404).json({ success: false, error: "checkInNotFound" });
  }
  catch (error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
};


export const checkOutController = async (req: Request, res: Response) => {
  /** Function for giving back a boat, adding the boat back to the available pool.
   *  User Optionally gives a comment, which is sent to the admins as an email.
   */
  try {
    if (!req.params.id || req.params.id == "undefined") {
      return res
        .status(404)
        .json({ success: false, error: "checkInIdNotFound" });
    }
    const checkin = await CheckIn.findByPk(req.params.id);
    if (!checkin) {
      return res
        .status(404)
        .json({ success: false, error: "checkInIdNotFound" });
    }

    let {
      note,
      bookingType,
    }: {
      note: string;
      bookingType: string;
    } = req.body;

    if ([undefined, null].includes(note) || note.trim() === "") {
      note = undefined;
    } else {
      note = note.trim();
    }

    const updatedCheckin = await CheckIn.update(
      {
        returned: true,
        note,
      },
      {
        where: {
          id: req.params.id,
        },
        returning: true,
      }
    );

    if (note !== undefined) {
      let checkIn = updatedCheckin[1][0];
      let checkInBoat = await Boat.findByPk(checkIn.BoatId);
      let boatName = "undefined";
      let checkInSport = await Sport.findByPk(checkIn.SportId);
      if (checkInBoat) {
        boatName = checkInBoat.name;
      }
      let boatManagers = await Employee.findAll({
        where: {
          role: "boatManager",
        }
      });

      let currentDate = new Date();
      boatManagers.forEach((wart: Employee) => {
        sendMail("notification", wart.email, `[Fahrtenbuch] Benachrichtigung für Boot ${boatName}`, {
          ["application_name"]: process.env.Application_Name,
          ["boat"]: boatName,
          ["notification"]: checkIn.note,
          ["author"]: checkIn.fullNameOfResponsibleClient,
          ["timestamp"]: `${new Date(checkIn.date).toLocaleDateString('de-DE', { weekday: 'short', month: 'short', day: 'numeric' })}`,
          ["startTime"]: `${checkIn.startTime.getHours().toString().padStart(2, "0")}:${checkIn.startTime.getMinutes().toString().padStart(2, "0")}`,
          ["returnedAt"]: `${currentDate.getHours().toString().padStart(2, "0")}:${currentDate.getMinutes().toString().padStart(2, "0")}`,
          ["sport"]: checkInSport.name,
        });
      })
    }
    return res
      .cookie("checkin_token", null, {
        secure: process.env.NODE_ENV === "production",
        path: "/",
        expires: new Date(0), // Sets date to past, so the cookie expires immediately
      })
      .status(200)
      .json({
        success: true,
        result: updatedCheckin,
      });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

export const getCheckInController = async (req: Request, res: Response) => {
  try {
    const checkin = await CheckIn.findByPk(req.params.id);
    if (checkin) {
      return res.status(200).json({ success: true, result: checkin });
    }
    return res.status(404).json({ success: false, error: "checkInIdNotFound" });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

export const getCheckInByBoatController = async (req: Request, res: Response) => {
  try {
    const checkIn = await CheckIn.findOne({
      where: {
        BoatId: req.params.id,
        returned: false,
      }
    });
    if (checkIn) {
      return res.status(200).json({ success: true, result: checkIn });
    }
    else {
      return res.status(404).json({ success: false, error: "boatNotCheckedOut" });
    }
  }
  catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
}

export const getCheckinsController = async (req: Request, res: Response) => {
  try {
    const checkIns = await CheckIn.findAll({
      include: [Boat, Accessory]
    });
    return res.status(200).json({ success: true, result: checkIns });
  }
  catch (error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
}

export const getCurrentCheckInController = async (
  req: Request,
  res: Response
) => {
  try {
    const currentId = res.locals.checkinToken.id; //payload
    const currentCheckIn = await CheckIn.findByPk(currentId);
    if (currentCheckIn) {
      return res.status(200).json({ success: true, result: currentCheckIn.id });
    } else {
      return res
        .status(404)
        .json({ success: false, error: "checkInIdNotFound" });
    }
  } catch (error) {
    res.status(500).json({ success: false, error: "serverError!" });
  }
};

export const resetCheckIns = async (req?: Request, res?: Response) => {

  const updatedCheckin = await CheckIn.update(
    {
      returned: true,
    },
    {
      where: {
        returned: false,
      },
      returning: true,
    }
  ); // Mitternachts wird aktualisiert: returned

};


export const getAllCurrentCheckinsController = async (req: Request, res: Response) => {
  try {
    // SELECT id, date, startTime, estimatedEndTime FROM CheckIn WHERE returned == False;
    const checkIns = await CheckIn.findAll({
      include: [Boat, Accessory],
      attributes: ["id", "date", "startTime", "estimatedEndTime"],
      where: { returned: false },
    });

    return res.status(200).json({ success: true, result: checkIns });
  }
  catch (error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
}

export const removeNamesInOldCheckIns = async (req?: Request, res?: Response) => {
  const fourWeeksAgo = new Date();
  fourWeeksAgo.setDate(fourWeeksAgo.getDate() - 28); // das Datum vor 4 Wochen

  const updateFourWeeksOldCheckin = await CheckIn.update(
    {
      fullNameOfResponsibleClient: " ",
      additionalClients: []
    },
    {
      where: {
        // fullNameOfResponsibleClient, email als zusätzliche Bedingung brauch ich nicht,
        // da, egal was drauf steht, nur auf das 'date' überprüft wird.
        date: {
          [Op.lt]: fourWeeksAgo,
        }, //Die Bedingung date: { [Op.lt]: fourWeeksAgo } bedeutet, dass die Spalte date kleiner sein muss als fourWeeksAgo. 
      },
      returning: true,
    }
  );
}


export const checkInAdminMemoController = async (req?: Request, res?: Response) => {

  let { adminMemo }: { adminMemo: string; } = req.body;

  if (adminMemo === "") {
    adminMemo = null;
  };

  try {
    const checkIn = await CheckIn.findByPk(req.params.id);
    if (checkIn) {
      await CheckIn.update({
        adminMemo: adminMemo,
      },
        {
          where: {
            id: req.params.id,
          },
          returning: false
        });
      return res.status(200).json({ success: true });
    }
    return res.status(404).json({ success: false, error: "checkInNotFound" });
  }
  catch (error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
}