import { Request, Response } from "express";
import CheckIn from "../db/models/CheckIn";
import Boat from "../db/models/Boat";
import BoatType from "../db/models/BoatType";
import Sport from "../db/models/Sport";

const showSport = async (req: Request, res: Response) => {
  try {
    const sport = await Sport.findByPk(req.params.id);
    if (!sport) {
      return res.status(404).json({ success: false, error: "sportIdNotFound" });
    }

    const checkins: any = await sport.getCheckIns({
      include: [{ model: Boat, include: [{ model: BoatType }] }]
    });
    let data = checkins.map((x: any) => { return { date: x.date, boatId: x.BoatId, boatName: x.Boat.name, boatTypeId: x.Boat.BoatType.id, boatTypeName: x.Boat.BoatType.name, hasNote: !!x.note, noteIsOpen: x.note && !x.noteDone} });


    return res.status(200).json({
      success: true, result: {
        sportName: sport.name,
        checkins: data
      }
    });

  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};
const showBoatType = async (req: Request, res: Response) => {
  try {
    const boatType = await BoatType.findByPk(req.params.id);
    if (!boatType) {
      return res.status(404).json({ success: false, error: "sportIdNotFound" });
    }
    let boats = await boatType.getBoats();
    let checkins:CheckIn[] = [];
    for (let b of boats) {
      let c = await b.getCheckIns({
        include: [{ model: Boat, include: [{ model: BoatType }] }, {model: Sport}]
      })
      checkins = checkins.concat(c)
    }
    let data = checkins.map((x: any) => { return { date: x.date, boatId: x.BoatId, boatName: x.Boat.name, boatTypeId: x.Boat.BoatType.id, boatTypeName: x.Boat.BoatType.name, hasNote: !!x.note, noteIsOpen: x.note && !x.noteDone} });
    return res.status(200).json({
      success: true, result: {
        boatTypeName: boatType.name,
        checkins: data
      }
    });

  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};
const showStatistic = async (req: Request, res: Response) => {
  try {
    const checkins = await CheckIn.findAll({
      include: [{ model: Boat, include: [{ model: BoatType }] }, {model: Sport}]
    });

    let data = checkins
      .filter(x=>x.BoatId && x.SportId)
      .filter((x: any) => x.Boat !== undefined || x.Boat.BoatType !== undefined)
      .map((x: any) => {
        return { 
          date: x.date,
          sportId: x.SportId,
          sportName: x.Sport.name,
          sportColor: x.Sport.color,
          boatId: x.BoatId,
          boatName: x.Boat.name,
          boatTypeId: x.Boat.BoatType.id,
          boatTypeName: x.Boat.BoatType.name,
          hasNote: !!x.note,
          noteIsOpen: x.note && !x.noteDone,
          additionalClients: x.additionalClients || [],
        } 
      });


    return res.status(200).json({
      success: true, result: {
        checkins: data
      }
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: error });
  }
};


const statisticControllers = {
  showSport,
  showBoatType,
  showStatistic
};

export default statisticControllers;
