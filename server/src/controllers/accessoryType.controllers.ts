import { Request, Response } from "express";
import AccessoryType from "../db/models/AccessoryType";
import Accessory from "../db/models/Accessory";
import BoatType from "../db/models/BoatType";

const createAccessoryTypeController = async (req: Request, res: Response) => {
  try {
    const { name, boattypes } = req.body;
    const newAccessoryType = await AccessoryType.create(
      { name }
    );
    let _boattypes: BoatType[] = await Promise.all((boattypes.map(async (x: { id: string }) => await BoatType.findByPk(x.id))))
    await newAccessoryType.setBoatTypes(_boattypes);

    return res.status(201).json({
      success: true,
      result: {
        id: newAccessoryType.id,
        name: newAccessoryType.name
      },
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const showAllAccessoryTypes = async (req: Request, res: Response) => {
  try {
    const allAccessoryTypes = await AccessoryType.findAll({
      attributes: ["id", "name"],
      include: BoatType
    });
    return res.status(200).json({
      success: true,
      result: allAccessoryTypes,
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const deleteAccessoryTypeById = async (req: Request, res: Response) => {
  try {
    const accessoryToDelete = await AccessoryType.destroy({
      where: {
        id: req.params.id,
      },
    });
    if (!accessoryToDelete) {
      return res
        .status(404)
        .json({ success: false, error: "accessoryTypeIdNotFound" });
    }
    return res.status(200).json({ success: true });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const showAccessoryTypeById = async (req: Request, res: Response) => {
  try {
    const accessory = await Accessory.findByPk(req.params.id);
    if (accessory) {
      const accessorytypeid = accessory.AccessoryTypeId;
      const accessoryType = await AccessoryType.findByPk(accessorytypeid);
      return res
        .status(200)
        .json({ success: true, result: accessoryType });
    }
    return res.status(404).json({ success: false, error: "accessoryIdNotFound" });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const updateAccessoryTypeById = async (req: Request, res: Response) => {
  try {
    const input = req.body;
    //return 200 with empty response if no data was given
    if (Object.keys(input).length === 0) {
      return res
        .status(200)
        .json({ success: true, result: {}, message: "noInputFound" });
    }

    //check if accessoryType can be found using givenId
    if (!await AccessoryType.findByPk(req.params.id)) {
      return res
        .status(404)
        .json({ success: false, error: "accessoryTypeIdNotFound" });
    }

    //try to update
    const updatedAccessoryType = await AccessoryType.update(input, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    if (input.boattypes) {
      let _boattypes: BoatType[] = await Promise.all((input.boattypes.map(async (x: { id: string }) => await BoatType.findByPk(x.id))))
      if (!(typeof updatedAccessoryType[1][0] == "number")) {
        await updatedAccessoryType[1][0].setBoatTypes(_boattypes);
      }
    }

    //return after updating
    const accessoryTypeDataAfterUpdate = updatedAccessoryType[1][0];
    return res.status(200).json({
      success: true,
      result: {
        id: accessoryTypeDataAfterUpdate.id,
        name: accessoryTypeDataAfterUpdate.name
      },
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const accessoryTypeControllers = {
  showAllAccessoryTypes,
  createAccessoryTypeController,
  deleteAccessoryTypeById,
  showAccessoryTypeById,
  updateAccessoryTypeById,
};

export default accessoryTypeControllers;
