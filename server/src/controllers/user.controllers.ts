import Employee from "../db/models/Employee";
import { Request, Response } from "express";
import bcrypt from "bcrypt";

//show current user GET
const showCurrentUserController = async (req: Request, res: Response) => {
  try {
    const currentUser = res.locals.user; //payload
    const currentUserData = await Employee.findOne({
      where: {
        email: currentUser.email,
      },
    });

    res.status(200).json({
      success: true,
      result: {
        id: currentUserData.id,
        first_name: currentUserData.first_name,
        last_name: currentUserData.last_name,
        email: currentUserData.email,
        role: currentUserData.role,
      },
    });
  } catch (error) {
    res.status(500).json({ success: false, error: "serverError!" });
  }
};

//update current user PATCH
const updateCurrentUser = async (req: Request, res: Response) => {
  try {
    //take what ever in req.body is, and pass it to update()
    const input = req.body; //Data are saved in the DB in camelCase

    //return 200 with empty response if no data was given
    if (Object.keys(input).length === 0) {
      return res
        .status(200)
        .json({ success: true, result: {}, message: "noInputFound" });
    }

    if (input.first_name !== undefined && input.first_name.length >= 30) {
      return res.status(400).json({
        success: false,
        result: {
          first_name: "Please provide maximum 30 character or empty string",
        },
      });
    }

    if (input.last_name !== undefined && input.last_name.length >= 30) {
      return res.status(400).json({
        success: false,
        result: {
          last_name: "Please provide maximum 30 character or empty string",
        },
      });
    }

    if (input.password !== undefined) {
      input.password = await bcrypt.hash(input.password, 10);
    }

    const currentUser = res.locals.user;
    const currentUserEmail = currentUser.email; //updating data of current user: payload in token has the unique email + role

    //update coordinator data after finding wanted user through found email
    const EmployeeDataAfterUpdate = await Employee.update(input, {
      where: {
        email: currentUserEmail,
      },
      returning: true,
    });

    const updatedEmployee = EmployeeDataAfterUpdate[1][0];
    delete updatedEmployee.password;

    return res.status(200).json({
      success: true,
      result: {
        id: updatedEmployee.id,
        first_name: updatedEmployee.first_name,
        last_name: updatedEmployee.last_name,
        email: updatedEmployee.email,
        role: updatedEmployee.role,
      },
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const userControllers = {
  showCurrentUserController,
  updateCurrentUser,
};

export default userControllers;
