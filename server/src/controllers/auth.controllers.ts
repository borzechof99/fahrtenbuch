import bcrypt from "bcrypt";
import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import Employee from "../db/models/Employee";
import envVars from "../config";

//log in
const authLoginController = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;

    const employee = await Employee.findOne({
      attributes: ["email", "password", "role"],
      where: {
        email: email,
      },
    });

    if (!employee) {
      return res.status(401).json({ success: false, error: "invalidEmail" });
    }

    const isPasswordCorrect = await bcrypt.compare(password, employee.password);

    if (isPasswordCorrect) {
      const payload = {
        email: employee.email,
        role: employee.role,
      };

      const token = jwt.sign(payload, envVars.JWT_SECRET);

      let cookieName = "token";
      let options: any = {
        httpOnly: true,
        secure: process.env.NOVE_ENV === "production",
        path: "/",
      };
      if (payload.role === "terminalUser") {
        options = { ...options, maxAge: 1000 * 60 * 60 * 24 * 14 }; // Two weeks!
      }

      return res
        .cookie(cookieName, token, options)
        .json({ success: true });
    } else {
      return res.status(401).json({ success: false, error: "invalidPassword" });
    }
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};
const authLogOutController = async (req: Request, res: Response) => {
  return res
    .cookie("token", null, {
      httpOnly: true,
      secure: process.env.NOVE_ENV === "production",
      path: "/",
      expires: new Date(0),
    })
    .json({ success: true });
};

const authControllers = {
  authLoginController,
  authLogOutController,
};

export default authControllers;
