import { Router } from "express";
import statisticControllers from "../controllers/statistics.controllers";
import { validateTokenGenerator } from "../middleware/validateToken";

const statisticRoute = Router();

statisticRoute.get("/api/statistic/sport/:id",validateTokenGenerator(), statisticControllers.showSport);
statisticRoute.get("/api/statistic/boattype/:id",validateTokenGenerator(), statisticControllers.showBoatType);
statisticRoute.get("/api/statistic/",validateTokenGenerator(), statisticControllers.showStatistic);


export default statisticRoute;
