import { Router } from "express";
import { body } from "express-validator";
import handleValidationResult from "../middleware/handleValidationResult";
import {
  checkInController,
  checkInNoteDoneController,
  checkInNoteNotDoneController,
  checkOutController,
  getCheckInByBoatController,
  getCheckInController,
  getCheckinsController,
  getCurrentCheckInController,
  getAllCurrentCheckinsController,
  checkInAdminMemoController,
} from "../controllers/createLogEntry.controllers";
import {
  validateCheckinToken,
  validateTokenGenerator
} from "../middleware/validateToken";

const entryRouter = Router();

entryRouter.post(
  "/api/checkin/",
  body("startTime").isISO8601(),
  body("estimatedEndTime").isISO8601(),
  body("destination").isString(),
  body("fullNameOfResponsibleClient").not().isEmpty(),
  body("additionalClients").if(body("additionalClients").exists()).isArray(),
  handleValidationResult,
  checkInController
);
entryRouter.post("/api/checkin/noteDone/:id", validateTokenGenerator(), checkInNoteDoneController);
entryRouter.post("/api/checkin/noteNotDone/:id", validateTokenGenerator(), checkInNoteNotDoneController);
entryRouter.post("/api/checkin/editmemo/:id", validateTokenGenerator(), checkInAdminMemoController);
entryRouter.post("/api/checkout/:id", checkOutController);

entryRouter.get("/api/checkin/:id", getCheckInController);

entryRouter.get("/api/checkin/boat/:id", validateTokenGenerator(), getCheckInByBoatController);

entryRouter.get("/api/checkins/", validateTokenGenerator(), getCheckinsController);

entryRouter.get(
  "/api/checkin/",
  validateCheckinToken,
  getCurrentCheckInController
);

entryRouter.get("/api/checkins/current", validateTokenGenerator(["terminalUser"]), getAllCurrentCheckinsController);

export default entryRouter;
