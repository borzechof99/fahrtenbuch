import { Router } from "express";
import { body } from "express-validator";
import handleValidationResult from "../middleware/handleValidationResult";
import authControllers from "../controllers/auth.controllers";

const authRouter = Router();

//log in route
authRouter.post(
  "/api/login/",
  body("email").isEmail().normalizeEmail(),
  body("password").not().isEmpty(),
  handleValidationResult,
  authControllers.authLoginController
);
authRouter.post("/api/logout/", authControllers.authLogOutController);

export default authRouter;
