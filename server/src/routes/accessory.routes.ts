import { Router } from "express";
import { body } from "express-validator";
import { validateTokenGenerator } from "../middleware/validateToken";
import accessoryControllers from "../controllers/accessory.controllers";
import handleValidationResult from "../middleware/handleValidationResult";

const accessoriesRouter = Router();

//show all boats
accessoriesRouter.get("/api/accessory/", accessoryControllers.showAllAccessoriesController);
accessoriesRouter.get("/api/accessoryoverview/", accessoryControllers.getAccessoryOverview);

//show boat by given id
accessoriesRouter.get("/api/accessory/:id", accessoryControllers.showAccessoryById);

//delete a boat
accessoriesRouter.delete(
  "/api/accessory/:id",
  validateTokenGenerator(),
  accessoryControllers.deleteAccessoryById
);

//create boat
accessoriesRouter.post(
  "/api/accessory/",
  body("name").not().isEmpty(),
  body("accessorytype").not().isEmpty(),
  body("status").not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  accessoryControllers.createAccessory
);

//update boat by id
accessoriesRouter.patch(
  "/api/accessory/:id/",
  body("name").if(body("name").exists()).not().isEmpty(),
  body("accessorytype").if(body("boattype").exists()).not().isEmpty(),
  body("status").if(body("status").exists()).not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  accessoryControllers.updateAccessoryById
);

//create boat
accessoriesRouter.post(
  "/api/accessory/lock/:id",
  handleValidationResult,
  validateTokenGenerator(),
  accessoryControllers.lock
);
//create boat
accessoriesRouter.post(
  "/api/accessory/unlock/:id",
  handleValidationResult,
  validateTokenGenerator(),
  accessoryControllers.unlock
);

export default accessoriesRouter;
