import { Router } from "express";
import { body } from "express-validator";
import accessoryTypeControllers from "../controllers/accessoryType.controllers";
import handleValidationResult from "../middleware/handleValidationResult";
import { validateTokenGenerator } from "../middleware/validateToken";
import isCoord from "../middleware/isCoord";

const accessoryTypeRouter = Router();

//show all accessoryTypes
accessoryTypeRouter.get(
  "/api/accessorytype/",
  validateTokenGenerator(),
  accessoryTypeControllers.showAllAccessoryTypes
);

//create accessoryType
accessoryTypeRouter.post(
  "/api/accessorytype/",
  body("name").not().isEmpty(),
  body("boattypes").if(body("boattypes").exists()).isArray().not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  accessoryTypeControllers.createAccessoryTypeController
);

//delete accessoryType by given id
accessoryTypeRouter.delete(
  "/api/accessorytype/:id",
  validateTokenGenerator(),
  accessoryTypeControllers.deleteAccessoryTypeById
);

//show specific accessory type by given accessory id
accessoryTypeRouter.get(
  "/api/accessorytype/:id/",
  validateTokenGenerator(),
  accessoryTypeControllers.showAccessoryTypeById
);

//update accessorytype by id
accessoryTypeRouter.patch(
  "/api/accessorytype/:id/",
  body("name").if(body("name").exists()).not().isEmpty(),
  body("boattypes").if(body("boattypes").exists()).isArray().not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  accessoryTypeControllers.updateAccessoryTypeById
);
export default accessoryTypeRouter;
