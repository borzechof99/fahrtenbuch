import { Router } from "express";
import { body } from "express-validator";
import sportControllers from "../controllers/sport.controllers";
import handleValidationResult from "../middleware/handleValidationResult";
import { validateTokenGenerator } from "../middleware/validateToken";
import isCoord from "../middleware/isCoord";

const sportRouter = Router();

//show all sports
sportRouter.get("/api/sport/", sportControllers.showAllSports);

//create a sport
sportRouter.post(
  "/api/sport/",
  body("name").not().isEmpty(),
  body("color").if(body("color").exists()).not().isEmpty(), //optional color field
  handleValidationResult,
  validateTokenGenerator(),
  sportControllers.createSportController
);

//delete Sport by given id
sportRouter.delete(
  "/api/sport/:id",
  validateTokenGenerator(),
  sportControllers.deleteSportById
);

//update a sport
sportRouter.patch(
  "/api/sport/:id/",
  body("name").if(body("name").exists()).not().isEmpty(),
  body("color").if(body("color").exists()).not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  sportControllers.updateSportById
);

//show sports assigned to boat id
sportRouter.get(
  "/api/sport/:id/",
  validateTokenGenerator(),
  sportControllers.showSportByBoatId
);

sportRouter.get(
  "/api/sports/:id",
  validateTokenGenerator(),
  sportControllers.showSportById
)

export default sportRouter;
