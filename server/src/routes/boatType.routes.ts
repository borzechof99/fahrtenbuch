import { Router } from "express";
import { body } from "express-validator";
import boatTypeControllers from "../controllers/boatType.controllers";
import handleValidationResult from "../middleware/handleValidationResult";
import { validateTokenGenerator } from "../middleware/validateToken";
import isCoord from "../middleware/isCoord";

const boatTypeRouter = Router();

//show all boatTypes
boatTypeRouter.get(
  "/api/boattype/",
  validateTokenGenerator(),
  boatTypeControllers.showAllBoatTypes
);

//create boatType
boatTypeRouter.post(
  "/api/boattype/",
  body("name").not().isEmpty(),
  body("seats").isInt({ min: 1 }).notEmpty(),
  body("sports").isArray().not().isEmpty(),
  body("maxTime").if(body("maxTime").exists()).not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  boatTypeControllers.createBoatTypeController
);

//delete BoatType by given id
boatTypeRouter.delete(
  "/api/boattype/:id",
  validateTokenGenerator(),
  boatTypeControllers.deleteBoatTypeById
);

//show specific boat type by given boat id
boatTypeRouter.get(
  "/api/boattype/:id/",
  validateTokenGenerator(),
  boatTypeControllers.showBoatTypeById
);

//update boattype by id
boatTypeRouter.patch(
  "/api/boattype/:id/",
  body("name").if(body("name").exists()).not().isEmpty(),
  body("seats").if(body("seats").exists()).isInt({ min: 1 }).notEmpty(),
  body("sports").if(body("sports").exists()).isArray().not().isEmpty(),
  body("maxTime").if(body("maxTime").exists()).not().isEmpty(),
  handleValidationResult,
  validateTokenGenerator(),
  boatTypeControllers.updateBoatTypeById
);
export default boatTypeRouter;
