import request from 'supertest';

describe('Serving Pages', () => {
	/*
	 * NOTICE: 
	 * Those are structural test examples.
	 * I've made it, that they all expect 404 errors, 
	 * since the database models have changes while the API hasn't
	 */
	it('should serve index page', (done) => {
		request("http://localhost:3000")
			.get('/')
			.set('Accept', 'text/html')
			.expect('Content-Type', /html/)
			.expect(200, done);
	}, 100000);
});