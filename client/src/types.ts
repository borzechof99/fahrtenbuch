import { Roles } from "./components/roles";

export interface Sport {
  id: string;
  name: string;
  color: string;
}
export interface Boat {
  id: string;
  name: string;
  status: number;
  boattype: string;
}
export interface OverviewBoat {
  id: string;
  name: string;
  status: number;
  maxTime: number;
  boattype: BoatType;
  currentCheckIn?: CheckIn;
}
export interface BoatType {
  id: string;
  name: string;
  seats: number;
  maxTime: number;
  Sports: Sport[];
  maxTimeMinutes?: number;
  maxTimeHours?: number;
}
export interface Account {
  id: string;
  first_name: string;
  last_name: string;
  password?: string;
  role: Roles;
  email: string;
}
export interface ID {
  id: string;
}
export interface CheckIn {
  id: string;
  sport: string;
  boatName: string;
  accessory: string | undefined;
  startTime: string;
  estimatedEndTime: string;
  destination: string;
  email: string;
  additionalClients: { passenger: string }[];
  fullNameOfResponsibleClient: string;
  returned: boolean;
  note: string;
  noteDone: boolean;
  date: string;
  Boat: Boat;
  Accessory: Accessory | undefined;
  termsAndConditions: boolean;
  adminMemo: string | undefined;
  isCourse: boolean;
}

export interface PublicCheckIn {
  id: string;
  startTime: string;
  estimatedEndTime: string;
  date: string;
  Boat: Boat;
}

export interface Accessory {
  id: string;
  name: string;
  status: number;
  accessorytype: string;
  accessorytypedata: AccessoryType | undefined;
}

export interface AccessoryType {
  id: string;
  name: string;
  BoatTypes: BoatType[];
}