export enum Roles {
    coordinator = "coordinator",
    boatManager = "boatManager",
    terminalUser = "terminalUser",
}

export function isValidRole(input: string): boolean {
    return Object.values(Roles).includes(input as any);
}